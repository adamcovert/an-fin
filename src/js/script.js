import ready from './utils/documentReady';
import {
  Swiper,
  Scrollbar
} from 'swiper';


Swiper.use([
  Scrollbar
]);

ready(() => {

  var breakpoint = window.matchMedia( '(min-width: 992px)' );
  var mySwiper;

  var breakpointChecker = function () {
    if ( breakpoint.matches === true ) {
  	  if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
	    return;
    } else if ( breakpoint.matches === false ) {
      return enableSwiper();
    }
  };

  var enableSwiper = function () {
    mySwiper = new Swiper('.price-list .swiper-container', {
      autoHeight: true,
      spaceBetween: 20,
      scrollbar: {
        el: '.price-list__scrollbar'
      },
      breakpoints: {
        320: {
          slidesPerView: 1
        },
        480: {
          slidesPerView: 2
        },
        768: {
          slidesPerView: 3
        },
        992: {
          slidesPerView: 4
        }
      }
    });
  };

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();
});